# Puppet-Ansible


# Automated SSH and Apache Installation using Puppet & Ansible

## Project Description

This project aims to automate the installation and configuration of SSH and Apache services on Ubuntu and CentOS systems using Puppet manifests and Ansible playbooks. The project is managed using Git and hosted on GitLab.

## Prerequisites

Ensure you have the following software installed:

- Git
- Puppet
- Ansible
- Ubuntu LTS (preferably on a virtual machine)

## Installation Instructions

1. **Clone the Repository:**

   git clone https://forge.univ-lyon1.fr/p2210385/puppet-ansible.git
   cd your-repo
Install Required Software:

sudo apt-get update
sudo apt-get install -y puppet ansible

# Configuration Instructions

## Puppet

Structure:

init.pp: Manages dependencies.
params.pp: Contains installation parameters.
install.pp: Handles package installations.
config.pp: Manages configuration files.
service.pp: Manages service state.

Apply Configuration:

sudo puppet apply --modulepath=~/Desktop/puppet-ansible/puppet/modules -e "include ssh"

## Ansible

Playbooks:

ssh.yml: Installs and configures SSH.

apache.yml: Installs and configures Apache.

Run Playbooks:

ansible-playbook ssh.yml
ansible-playbook apache.yml

## Git Branch Structure

ansible-dev: Development branch for Ansible playbooks.
puppet-dev: Development branch for Puppet manifests.
ansible-prod: Production branch for Ansible playbooks.
puppet-prod: Production branch for Puppet manifests.
main: Main branch containing the README and overall project structure.

## Displaying System Information with Apache

Modify the Apache Module:

In the puppet-web branch, edit the appropriate Puppet templates to display system information. You can use the facter tool to retrieve system facts.

Example Template Modification:

erb
Copier le code
<html>
  <head>
    <title>System Information</title>
  </head>
  <body>
    <h1>System Information</h1>
    <p>Uptime: <%= Facter.value(:uptime) %></p>
    <p>Current Time: <%= Time.now %></p>
    <p>Operating System: <%= Facter.value(:osfamily) %> <%= Facter.value(:operatingsystem) %> <%= Facter.value(:operatingsystemrelease) %></p>
  </body>
</html>

Deploy the Updated Module:

sudo puppet apply --modulepath= ~/Desktop/puppet-ansible/puppet/modules-e "include apache"

Access the Website:

Once deployed, you can access the website displaying system information at the server's IP address or hostname.

http://your-server-ip-or-hostname

# Comparison of Tools

Ease of Learning:

Puppet has a more declarative syntax, which might be easier for those familiar with configuration management.
Ansible uses YAML, which is straightforward and widely used.
Configuration Management:

Puppet uses Hiera for hierarchical data management.
Ansible uses inventory files to manage host configurations.

Flexibility and Modularity:

Puppet modules can be reused and shared.
Ansible roles provide a similar level of modularity.
Use Cases and Performance:

Puppet is well-suited for long-term infrastructure management.
Ansible is excellent for task automation and ad-hoc configurations.
By following this README, you will be able to set up an automated environment for managing SSH and Apache installations using Puppet and Ansible, with a structured Git workflow to manage development and production environments.


This README file should provide a clear and concise guide for setting up and managing your project. 